<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/', function () {
		return view('welcome');
	});
	
	Route::get('/second-buyer-eloquent', 'ItechTestController@secondBuyerEloq');
	Route::get('/second-buyer-no-eloquent', 'ItechTestController@secondBuyerNoEloq');
	
	
	Route::get('/purchase-list-eloquent', 'ItechTestController@purchaseListEloq');
	Route::get('/purchase-list-no-eloquent', 'ItechTestController@purchaseListNoEloq');
	

	Route::get('/define-callback-js', 'ItechTestController@definedCallback');
	Route::get('/animation', 'ItechTestController@animateBox');	
	Route::get('/records', 'ItechTestController@records');
	
	Route::get('/sort-js', 'ItechTestController@sortJs');
	Route::get('/filter-js', 'ItechTestController@filterJs');	
	Route::get('/foreach-js', 'ItechTestController@foreachJs');
	Route::get('/map-js', 'ItechTestController@mapJs');	
	Route::get('/reduce-js', 'ItechTestController@reduceJs');
