<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    Purchase list with no eloq
                </div>
				 
				<table class="table" cellpadding="5" cellspacing="5">
				<thead>
				<tr>
					<td>Buyer Id</td>
					<td>Buyer Name</td>
					<td>Total Pen Taken</td>
				    <td>Total Diary Taken</td>
					<td>Total Eraser Taken</td>
				</tr>
				</thead>
				<tbody>			
				@foreach($purchaseList as $p)				
				<tr>
					<td>{{ $p->id }}</td>
					<td>{{ $p->name }}</td>
					<td>{{ $p->total_pen_taken }}</td>
				    <td>{{ $p->total_diary_taken }}</td>
					<td>{{ $p->total_eraser_taken }}</td>
				</tr>
				@endforeach
				<tbody>
				</table>
				
			    
            </div>
        </div>
    </body>
</html>
