<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    Defined Callback 
                </div>
				<script>
					var data = {email:'trump@gmail.com', age:70}; // input json.	
					checkAge = (data, callback1) => {
						callback1(data);	
					};
					checkAge(data, (x) => { 
						return x.age < 18 ? console.log('not valid') : console.log('valid');		
					});
				</script>
                
            </div>
        </div>
    </body>
</html>
