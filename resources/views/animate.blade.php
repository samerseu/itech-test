<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    Animate
                </div>
				<script>
					 $(document).ready(function(){
						$("button").click(function(){
							var div = $("div");
							startAnimation();
							function startAnimation(){
							  div.animate({height: 150}, "slow");
							  div.animate({width: 150}, "slow");
							  //div.css("background-color", "blue");  
							  div.animate({height: 100}, "slow");
							  div.animate({width: 100}, "slow", startAnimation);
							}
						 
						});
					 });	
				</script>
				
				
			<button id="btn1">Start Animation</button>
			<div style="width:50px;height:50px;position:absolute;left:10px;top:50px;background-color:red;"></div>
                
            </div>
        </div>
    </body>
</html>
