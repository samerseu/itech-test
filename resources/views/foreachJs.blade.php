<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    sort-js 
                </div>
				<script>
				 
					/// array sort with loop
					var input = [2,3,8,1,4,5,9,7,6];
					var output = [];
					var myCheck;					
					for (var i = 0, ii = input.length ; i < ii ; i++){
					  myCheck = false;
					  for (var j = 0, jj = output.length ; j < jj ; j++){
						if (input[i] < output[j]){
						  myCheck = true;
						  output.splice(j, 0, input[i]);
						  break;
						}
					  }
					  
					  if (!myCheck)
						output.push(input[i])
					}	
					console.log('-sort with loop');					
					console.log(output);
					
					 
				</script>
				
				
			    
            </div>
        </div>
    </body>
</html>
