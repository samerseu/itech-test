<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EraserTaken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('eraser_taken', function (Blueprint $table) {
            $table->increments('id' , 5);
            $table->integer('buyer_id' , 5)->default(null);
			$table->integer('amount' , 10)->default(null);            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eraser_taken');
    }
}
