<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Records;
use App\Buyers;
use App\PenTaken;
use App\EraserTaken;
use App\DiaryTaken;

class ItechTestController extends Controller
{        



	public function secondBuyerEloq() {
		$purchaseList = Buyers::select([
								'buyers.id',
								'buyers.name',
								\DB::raw('count(pen_taken.id) as total_pen_taken'),
								\DB::raw('count(eraser_taken.id) as total_eraser_taken'), 
								\DB::raw('count(diary_taken.id) as total_diary_taken'),								
								\DB::raw('count(pen_taken.id)+count(eraser_taken.id)+count(diary_taken.id) as total_taken'),
								 
								])
						->leftJoin('pen_taken', 'pen_taken.buyer_id' ,'=','buyers.id')
						->leftJoin('eraser_taken', 'eraser_taken.buyer_id' ,'=','buyers.id')
						->leftJoin('diary_taken', 'diary_taken.buyer_id' ,'=','buyers.id')
						->groupBy('buyers.id','buyers.name')
						->having('total_taken', '<', \DB::raw('max(total_taken)'))					
						->orderBy('total_pen_taken', 'asc')						
						->get();
						
						
		
		// dd($purchaseList);
		 
		return view('itech-test.second_buyer_list_eloq')
				->with('purchaseList', $purchaseList);
  
	}
	
	
	public function secondBuyerNoEloq() {
		$query = \DB::raw('select b.id,b.name ,  
						count(p.id) as total_pen_taken,  
						count(e.id) as total_eraser_taken, 
						count(d.id) as total_diary_taken ,
						(count(p.id)+count(e.id)+count(d.id))  as total_taken  
					from 
						 buyers b 
						 left JOIN pen_taken p on (p.buyer_id = b.id) 
						 LEFT join eraser_taken e on (e.buyer_id = b.id)
						 LEFT JOIN diary_taken d on (d.buyer_id = b.id)
						 GROUP by b.id , b.name
						 HAVING total_taken < max(total_taken)
						 order by total_pen_taken asc');
			 
		$purchaseList = \DB::select($query);		 
		//dd($purchaseList);
		return view('itech-test.second_buyer_list_no_eloq')
				->with('purchaseList', $purchaseList);
	}



    public function purchaseListEloq() {
		$purchaseList = Buyers::select(['buyers.id','buyers.name',
		\DB::raw('count(pen_taken.id) as total_pen_taken')
		, \DB::raw('count(eraser_taken.id) as total_eraser_taken', 
		\DB::raw('count(diary_taken.id) as total_diary_taken'))])
		->leftJoin('pen_taken', 'pen_taken.buyer_id' ,'=','buyers.id')
		->leftJoin('eraser_taken', 'eraser_taken.buyer_id' ,'=','buyers.id')
		->leftJoin('diary_taken', 'diary_taken.buyer_id' ,'=','buyers.id')
		->groupBy('buyers.id','buyers.name')
		->orderBy('total_pen_taken', 'asc')
		->get();
		//dd($purchaseList);
		
		return view('purchase_list_eloq')
				->with('purchaseList', $purchaseList);
 
	}
	
	 public function purchaseListNoEloq() {
		$query = \DB::raw('select b.id,b.name ,  count(p.id) as total_pen_taken,  
		count(e.id) as total_eraser_taken, count(d.id) as total_diary_taken from 
			 buyers b 
			 left JOIN pen_taken p on (p.buyer_id = b.id) 
			 LEFT join eraser_taken e on (e.buyer_id = b.id)
			 LEFT JOIN diary_taken d on (d.buyer_id = b.id)
			 GROUP by b.id , b.name
			 order by total_pen_taken asc');
			 
		$purchaseList = \DB::select($query);
		//dd($purchaseList);
		
		return view('purchase_list_no_eloq')
				->with('purchaseList', $purchaseList);
	}
	
	
	public function definedCallback() {
	   return view("defined-callback");
    }
    	
	public function records() {		
		ini_set('allow_url_fopen', '1');
		echo $filePath = storage_path('app/public/records.json'); 
		$data =  json_decode(file_get_contents($filePath), true);			 
		
		Records::insert($data['RECORDS']);				 	    
		dd($data['RECORDS']);
    }
	
	
	 public function animateBox() {
	   return view("animate");
    }
	
	
	public function sortJs() {
		return view("sortJs");
	}
	
	public function filterJs() {
		return view("filterJs");
	}
	
	public function foreachJs() {
		return view("foreachJs");
	}
	
	public function mapJs() {
		return view("mapJs");
	}
	
	public function reduceJs() {
		return view("reduceJs");
	}
	    
	
	 
}


 