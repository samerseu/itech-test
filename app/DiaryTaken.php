<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiaryTaken extends Model
{
     protected $fillable = [
        		'id',
			    'buyer_id',			    
			    'amount' 
			     
    ];
    
    protected $table = 'diary_taken';
    protected $primaryKey = 'id';
}
