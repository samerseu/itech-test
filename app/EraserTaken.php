<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EraserTaken extends Model
{
     protected $fillable = [
        		'id',
			    'buyer_id',			    
			    'amount' 
			     
    ];
    
    protected $table = 'eraser_taken';
    protected $primaryKey = 'id';
}
