<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenTaken extends Model
{
     protected $fillable = [
        		'id',
			    'buyer_id',			    
			    'amount' 
			     
    ];
    
    protected $table = 'pen_taken';
    protected $primaryKey = 'id';
}
