<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyers extends Model
{
     protected $fillable = [
        		'id',
			    'name',
			    
			    'status' 
			     
    ];
    
    protected $table = 'buyers';
    protected $primaryKey = 'id';
}
